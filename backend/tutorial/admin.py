from django.contrib import admin
from .models import Tutorial


class TutorialAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_at']
    list_filter = ['created_at']


admin.site.register(Tutorial, TutorialAdmin)