from django.urls import path
from .views import TutorialListApiView, TutorialDetailView

urlpatterns = [
    path('tutorials/', TutorialListApiView.as_view(), name='tutorials-list'),
    path('<int:pk>/detail/', TutorialDetailView.as_view(), name='tutorial-detail')
]