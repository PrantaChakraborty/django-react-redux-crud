from django.shortcuts import render
from rest_framework import generics
from .models import Tutorial
from .serializers import TutorialSerializer

# Create your views here.


class TutorialListApiView(generics.ListCreateAPIView):
    """
        view for create tutorials and list of tutorials
    """
    queryset = Tutorial.objects.all()
    serializer_class = TutorialSerializer


class TutorialDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
        view for retrieve update and delete
    """
    queryset = Tutorial.objects.all()
    serializer_class = TutorialSerializer
