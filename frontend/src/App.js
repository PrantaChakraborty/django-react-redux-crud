import React, { Component } from "react"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"
import TutorialList from "./components/TutorialList"
import Tutorial from "./components/Tutorial"
import AddTutorial from "./components/AddTutorial"
class App extends Component {
	render() {
		return (
			<div>
				<Router>
					<nav className="navbar navbar-expand navbar-dark bg-dark">
						<div className="navbar-nav mr-auto">
						
							<li className="nav-item">
								<Link to="/tutorials" className="nav-link">
									Tutorial List
								</Link>
							</li>
							<li className="nav-item">
								<Link to="/add" className="nav-link">
									Add Tutorial
								</Link>
							</li>
						</div>
					</nav>
					<Switch>
						<Route
							exact
							path={["/", "/tutorials"]}
							component={TutorialList}
						/>
						<Route exact path="/add" component={AddTutorial} />
						<Route path="/tutorials/:id" component={Tutorial} />
					</Switch>
				</Router>
			</div>
		)
	}
}

export default App
