import {
	CREATE_TUTORIAL,
	RETRIEVE_TUTORIAL,
	UPDATE_TUTORIAL,
	DELETE_TUTORIAL,
} from "./types"

import TutorialDataService from "../service"

export const createTutorial = (title, description) => async (dispatch) => {
	//  action for create tutorial
	try {
		const res = await TutorialDataService.create({ title, description })

		dispatch({
			type: CREATE_TUTORIAL,
			payload: res.data,
		})
		return Promise.resolve(res.data)
	} catch (err) {
		return Promise.reject(err)
	}
}

export const retrieveTutorials = () => async (dispatch) => {
	// action for list of tutorials
	try {
		const res = await TutorialDataService.getAll()
		dispatch({
			type: RETRIEVE_TUTORIAL,
			payload: res.data,
		})
	} catch (err) {
		console.log(err)
	}
}

export const updateTutorial = (id, data) => async (dispatch) => {
	// action for update
	try {
		const res = await TutorialDataService.update(id, data)
		dispatch({
			type: UPDATE_TUTORIAL,
			payload: data,
		})
		return Promise.resolve(res.data)
	} catch (err) {
		return Promise.reject(err)
	}
}

export const deleteTutorial = (id) => async (dispatch) => {
    // action for delete
	try {
		await TutorialDataService.delete(id)
		dispatch({
			type: DELETE_TUTORIAL,
			payload: { id },
		})
	} catch (err) {
		console.log(err)
	}
}
