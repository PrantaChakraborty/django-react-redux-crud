import React, { Component } from "react"
import { connect } from "react-redux"
import { createTutorial } from "../actions/TutorialActions"

class AddTutorial extends Component {
	constructor(props) {
		super(props)
		this.state = {
			title: "",
			description: "",
			submitted: false,
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.newTutorial = this.newTutorial.bind(this)
	}

	handleChange(event) {
		event.preventDefault()
		this.setState({ [event.target.name]: event.target.value })
	}

	handleSubmit() {
		const { title, description } = this.state
		this.props
			.createTutorial(title, description)
			.then((data) => {
				this.setState({
					title: data.title,
					description: data.description,
					submitted: true,
				})
				console.log(data)
			})
			.catch((e) => {
				console.log(e)
			})
	}

	newTutorial() {
		this.setState({
			title: "",
			description: "",
			submitted: false,
		})
	}
	render() {
		return (
			<div className="submit-form">
				{this.state.submitted ? (
					<div>
						<h3>You submitted successfully</h3>
						<button
							className="btn btn-success"
							onClick={this.newTutorial}
						>
							Add
						</button>
					</div>
				) : (
					<div>
						<form onSubmit={this.handleSubmit}>
							<div className="container">
								<div className="form-group ">
									<label htmlFor="title">Title</label>
									<input
										type="text"
										className="form-control"
										id="title"
										required
										value={this.state.title}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="description">
										description
									</label>
									<input
										type="text"
										className="form-control"
										id="description"
										required
										value={this.state.description}
										onChange={this.handleChange}
									/>
								</div>

								<button className="btn btn-success mt-2">
									Submit
								</button>
							</div>
						</form>
					</div>
				)}
			</div>
		)
	}
}
export default connect(null, { createTutorial })(AddTutorial)
