import React, { Component } from "react"
import { connect } from "react-redux"
import { updateTutorial, deleteTutorial } from "../actions/TutorialActions"
import TutorialDataService from "../service"

class Tutorial extends Component {
	constructor(props) {
		super(props)
		this.handleChangeTitle = this.handleChangeTitle.bind(this)
		this.handleChangeDescription = this.handleChangeDescription.bind(this)
		this.getTutorial = this.getTutorial.bind(this)
		this.removeTutorial = this.removeTutorial.bind(this)
		// this.updateStatus = this.updateStatus.bind(this)
		this.updateContent = this.updateContent.bind(this)

		this.state = {
			currentTutorial: {
				title: "",
				description: "",
			},
			message: "",
		}
	}
	componentDidMount() {
		this.getTutorial(this.props.match.param.id)
	}
	handleChangeTitle(e) {
		const title = e.target.value
		this.setState((prevState) => {
			return {
				currentTutorial: {
					...prevState.currentTutorial,
					title: title,
				},
			}
		})
	}
	handleChangeDescription(e) {
		const description = e.target.description
		this.setState((prevState) => {
			return {
				currentTutorial: {
					...prevState.currentTutorial,
					description: description,
				},
			}
		})
	}

	getTutorial(id) {
		TutorialDataService.get(id)
			.then((response) => {
				this.setState({
					currentTutorial: response.data,
				})
				console.log(response.data)
			})
			.catch((error) => {
				console.log(error)
			})
	}

	// updateStatus(status) {
	// 	var data = {
	// 		title: this.state.currentTutorial.id,
	// 		description: this.state.currentTutorial.description,
	// 	}
	// 	this.props
	// 		.updateTutorial(this.state.currentTutorial.id, data)
	// 		.then((response) => {
	// 			console.log(response)
	// 			this.setState((prevState) => ({
	// 				currentTutorial: {
	// 					...prevState.currentTutorial,
	// 				},
	// 			}))
	//             this.setState({message: 'The status was'})
	// 		})
	// }

	updateContent() {
		this.props
			.updateTutorial(
				this.state.currentTutorial.id,
				this.state.currentTutorial
			)
			.then((response) => {
				console.log(response)
				this.setState({
					message: "The tutorial was updated successfully!",
				})
			})
			.catch((e) => {
				console.log(e)
			})
	}

	removeTutorial() {
		this.props
			.deleteTutorial(this.state.currentTutorial.id)
			.then(() => this.props.history.push("/tutorials"))
			.catch((e) => {
				console.log(e)
			})
	}

	render() {
		const { currentTutorial } = this.state
		return (
			<div>
				<p>Tutorial</p>
			</div>
		)
	}
}

export default Tutorial
