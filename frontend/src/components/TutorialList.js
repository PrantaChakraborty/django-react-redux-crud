import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { retrieveTutorials } from "../actions/TutorialActions"

class TutorialList extends Component {
	constructor(props) {
		super(props)
		this.state = {
			currentTutorial: null,
			currentIndex: -1,
		}
		this.refreshData = this.refreshData.bind(this)
		this.setActiveTutorial = this.setActiveTutorial.bind(this)
	}
	componentDidMount() {
		this.props.retrieveTutorials()
	}
	refreshData() {
		this.setState({
			currentTutorial: null,
			currentIndex: -1,
		})
	}
	setActiveTutorial(tutorial, index) {
		this.setState({
			currentTutorial: tutorial,
			currentIndex: index,
		})
	}

	render() {
		const { currentTutorial, currentIndex } = this.state
		const { tutorials } = this.state

		return (
			<div className="list row">
				<div className="col md-6">
					<h3>Tutorial Lists</h3>
					<ul className="list-group">
						{tutorials &&
							tutorials.mpa((tutorial, index) => {
								;<li
									className={
										"list-group-item" +
										(index === currentIndex)
											? "active"
											: ""
									}
									onClick={() =>
										this.setActiveTutorial(tutorial, index)
									}
									key={index}
								>
									{tutorial.title}
								</li>
							})}
					</ul>
				</div>
				<div className="col md-6">
					{currentTutorial ? (
						<div>
							<h4>Tutorial</h4>
							<div>
								<label>
									<strong>Title</strong>
								</label>
								{currentTutorial.title}
							</div>
							<div>
								<label>
									<strong>Description</strong>
								</label>
								{currentTutorial.description}
							</div>
							<Link
								to={"/tutorials/" + currentTutorial.id}
								className="badge badge-warning"
							>
								Edit
							</Link>
						</div>
					) : (
						<div>
							<br />
							<p>Please Click on a tutorial...</p>
						</div>
					)}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		tutorials: state.tutorials,
	}
}

export default connect(mapStateToProps, { retrieveTutorials })(TutorialList)
