import {
	CREATE_TUTORIAL,
	RETRIEVE_TUTORIAL,
	UPDATE_TUTORIAL,
	DELETE_TUTORIAL,
} from "../actions/types"

const initailState = []

function tutorialReducer(state = initailState, action) {
	const { type, payload } = action
	switch (type) {
		case CREATE_TUTORIAL:
			return [...state, payload]

		case RETRIEVE_TUTORIAL:
			return payload

		case UPDATE_TUTORIAL:
			return state.map((tutorial) => {
				if (tutorial.id === payload.id) {
					return {
						...tutorial,
						...payload,
					}
				} else {
					return tutorial
				}
			})

		case DELETE_TUTORIAL:
			return state.fillter(({ id }) => id !== payload.id)

        default:
            return state
	}
}

export default tutorialReducer
