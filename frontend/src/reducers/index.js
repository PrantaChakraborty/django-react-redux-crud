import { combineReducers } from "redux"
import tutorialReducer from "./TutorialReducers"

export default combineReducers({
	// to combine the reducer in store
	tutorials: tutorialReducer,
})
