//  all url point

import http from './axiosInstance'

class TutorialDataService{
    getAll(){
        // return the list of all tutorials
        return http.get('/tutotials')
    }

    get(id){
        // return single tutorial
        return http.get(`${id}/detail`)
    }
    create(data){
        // create tutorial
        return http.post('/tutorials', data)
    }
    update(id, data){
        // update
        return http.put(`${id}/detail`, data)
    }
    delete(id){
        // delete
        return http.delete(`${id}/detail`)
    }
}

export default TutorialDataService